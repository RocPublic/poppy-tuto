Texter
=============================================

One Paragraph of plugin description goes here


Installation
------------

To install Texter:

1.  Install with pip: ``pip install tuto.texter``.
2.  In ``settings.py``, add ``'tuto.texter'`` to ``PLUGINS``.
3.  Run ``manage.py piper descriptor``.

Authors
-------

- Toto

License
-------

This project is licensed under the Toto License

Acknowledgments
---------------

-  Hat tip to anyone who’s code was used
-  Inspiration
-  etc