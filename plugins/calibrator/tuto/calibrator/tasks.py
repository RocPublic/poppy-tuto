import json
import os.path as osp
import re

from poppy.pop.plugins import Plugin
from poppy.core.logger import logger
from poppy.pop.target import create_target


__all__ = ['calibrate']


CalibrateTask = Plugin.manager['tuto.calibrator'].task('calibrate')


@CalibrateTask.as_task
def calibrate(task):
    """
    Do my job
    """
    logger.info('In calibrate() task')


    with task.pipeline.properties.text.open() as file:
        text = str(json.loads(file.readline()))

    # capitalize first letter of the text
    text = text.capitalize()

    # find all 'dot' characters
    dot_position = [m.start() for m in re.finditer('\.', text)]

    # capitalize every letter just after a dot
    text_list = list(text)
    for pos in dot_position:
        letter = text_list[pos+2]
        text_list[pos+2] = letter.upper()

    text_list.append('.')
    cal_text = ''.join(text_list)


    target = create_target(task, 'cal_text')

    with target.open("w") as f:
        f.write(cal_text)
