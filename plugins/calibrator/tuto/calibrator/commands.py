# from poppy.core.command import Command
# from poppy.pop import Pop
# from .tasks import my_task1, my_task2, my_task3
#
# __all__ = ["CalibratorDoSomeStuffCommand", ]
#
# class CalibratorDoSomeStuffCommand(Command):
#     """
#     My job command description
#     """
#     __command__ = "calibrator_do_some_stuff"
#     __command_name__ = "do_some_stuff"
#     __parent__ = "calibrator"
#     __parent_arguments__ = ["base"]
#     __help__ = """
#         Command to do some stuff
#     """
#
#     def add_arguments(self, parser):
#         # my arg1
#         parser.add_argument(
#             '--my_arg1',
#             help="""
#             Arg1 help
#             """,
#         )
#
#         # my arg2
#         parser.add_argument(
#             '-m2', '--my_arg2',
#             help="""
#             Arg2 help
#             """,
#         )
#
#     def __call__(self, args):
#         # create the pipeline
#         pipeline = Pop(args)
#
#         # starting task
#         start = my_task1()
#         end = my_task3()
#
#         # get the class for constructing the target for XML
#         Target = start.input_target("my_arg1")
#
#         # create target instance
#         target = Target(args.my_arg1)
#
#         # set it into the pipeline
#         pipeline.properties.my_arg1 = target
#
#         # Set the arg2 property
#         pipeline.properties.my_arg2 = args.my_arg2
#
#         # setup the pipeline topology
#         pipeline | (
#             start | my_task2() | end
#         )
#
#         # define the start points of the pipeline
#         pipeline.start = start
#         pipeline.end = end
#
#         # run the pipeline
#         pipeline.run()