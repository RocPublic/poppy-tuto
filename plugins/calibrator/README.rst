Calibrator
=============================================

One Paragraph of plugin description goes here


Installation
------------

To install Calibrator:

1.  Install with pip: ``pip install tuto.calibrator``.
2.  In ``settings.py``, add ``'tuto.calibrator'`` to ``PLUGINS``.
3.  Run ``manage.py piper descriptor``.

Authors
-------

- Toto

License
-------

This project is licensed under the Toto License

Acknowledgments
---------------

-  Hat tip to anyone who’s code was used
-  Inspiration
-  etc