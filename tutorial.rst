********
Tutorial
********


Imagine you want to communicate with your friend located far away from you. You
cannot send the text directly because your bandwidth is really small. You could
both take the same dictionary, and send to each other a sequence of integers
representing the index of the word in the dictionary.

Checking manually the index in a dictionary is really time consuming so you
decide to write a software to do the work for you.

You design your software like this :

.. _example:

.. figure:: images/example.*
    :width: 75%
    :align: center

    + :code:`Packets` is the input data, it is lists of integers
+ *decommute* is a function replacing each integers with its corresponding
  word in the dictionary
+ the dictionary is stored in a database, that we need to fill beforehand
+ *make_text* takes list of words and create a simple text
+ *calibrate* takes the simple text and format it

The goal is to have two available command :
- load the dictionary into the database
- generate a text file from a input packet file


Set up your development environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The POPPy framework is to be used with `Python’s virtual environments
<https://docs.python.org/3/tutorial/venv.html>`_ to avoid dependancy conflicts.

To create a virtualenv and then source it, you can use :


.. code-block:: bash

   $ python3 -m venv myprojectvenv
   $ source myprojectvenv/bin/activate

Then install POPPyCore

.. code-block:: bash

   $ pip install git+https://gitlab.obspm.fr/RocPublic/poppy-tuto.git@develop#egg=poppy.core


TBW : system dependancies




Create a new plugin
~~~~~~~~~~~~~~~~~~~


First, we need to create a plugin that will *decommute* our list of packets
into a text. Let's call it **texter**.

Task creation
=============

Adding the task to the descriptor
---------------------------------

The first thing to do is to register your task into your plugin descriptor.
We want to create the task that will fill the database with the dictionary.
Open the file :code:`descriptor.json` inside the tuto.texter plugin directory,
and replace the :code:`tasks` attribute with the following :

.. code-block:: json

    "tasks": [
        {
            "name": "load_dict",
            "category": "Software execution",
            "description": "Loads the dictionary in the database",
            "inputs": {},
            "outputs": {}
        }
    ]

We just told the framework that our plugin tuto.texter has a task called
load_dict, with no inputs nor outputs.


Create the task
---------------

If a plugin following the pipeline interface is defined and activated, it can
be used to define a task. For example, if in the descriptor of the plugin (see
:ref:`plugin_descriptor`) is defined a task called :code:`load_dict` with the
good software category, description, etc, you can simply create a task from
this definition. In the tuto.texter :code:`tasks.py` file, we can write:

.. code-block:: python

    from poppy.pop.plugins import Plugin

    # create the class of the task from the definitions of the pipeline
    LoadTask = Plugin.manager['tuto.texter'].task('load_dict')

    # instantiation of the task
    @LoadTask.as_task
    def load_dict(task):
        """
        Load the dictionary into the table 'dictionary' of the database
        """
        pass

.. note::
    Tasks through plugins contains also extended functionalities allowing for
    example to define *targets* simply by name from their definition in the
    descriptor. Refer to the section :ref:`targets` for details and description
    of functionalities.


In reality, there are 3 ways of defining a task. You can find all of them in
the dedicated section of the developer's guide.


Then create a file in the :code:`lib/` directory of the pipeline called
:code:`dictionary.txt` containing:

.. code-block::
    awesome
    framework
    great
    is
    poppy
    python


Then in the :code:`load_dict` function, add the code that reads this file and
put its content into the database:

.. code-block:: python

    LoadTask = Plugin.manager['tuto.texter'].task('load_dict')

    @LoadTask.as_task
    def load_dict(task):
        """
        Load the dictionary into the table 'dictionary' of the database
        """

        # get the sqlalchemy session needed to communicate with the database
        session = task.pipeline.roc.session

        dictionary = list()

        # build the path to the dictionary file
        dict_file_path = os.path.join(settings.ROOT_DIRECTORY,
                                      'lib',
                                      'dictionary.txt')

        # open the file and build a list element for each line
        with open(dict_file_path, 'r') as file:
            for line in file:
                dictionary.append(line.strip('\n'))

        # create a database entry in the dictionary table for each word
        for i, word in enumerate(dictionary):
            dict_entry = get_or_create(
                session,
                Dictionary,
                identifier=i,
                word=word
            )

            print(f'ID = {dict_entry.identifier} ; word = {dict_entry.word}')


The interesting part here is the for loop. For each element of the list built
from the dictionary file, we create an entry in the database with the index in
the list as :code:`identifier` (which is the primary key according to our model).
The function :code:`get_or_create()` is a POPPy wrapper used either to create
elements or get from the database. This function solves eventual concurrency
problems.


Command creation
================

