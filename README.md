### Set up the tutorial environment

- Clone this repo
- Create a virtualenv and install requirements

```
$- python3.6 -m venv ~/.virtualenvs/tuto_poppy
$- source ~/.virtualenvs/tuto_poppy/bin/activate
$- cd poppy_tuto
$- pip install -r requirements.txt
```
- Create a postgresql database, for example:

```
$- sudo -u postgres -i
$- psql
postgres=# create user pipeuser with password 'userpwd';
postgres=# create user pipeadmin with password 'adminpwd';
postgres=# create database poppy_tuto with owner pipeadmin;
```

- Run the migrations

> $- python manage.py db upgrade heads

Now you can run the demo !

### Use the pipeline
- Populate the database with the descriptor

> $- python manage.py piper descriptor

- Populate the database with the dictionary

> $- python manage.py texter load

- Run the reconstruction of the text

> $- python manage.py texter text_from_packet --packet_file packets.txt
