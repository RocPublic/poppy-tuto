#!/usr/bin/env python3

import os
import sys
import settings
from poppy.core.conf import settings as pipeline_settings

if __name__ == "__main__":
    # Add plugins dir to the python path
    sys.path.insert(0, os.path.join(settings.ROOT_DIRECTORY, 'plugins'))

    pipeline_settings.configure(settings)

    # run the pipeline
    try:
        from poppy.pop.scripts import main
    except ImportError as exc:
        raise ImportError(
            "Couldn't import poppy-pop. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc


    main()


